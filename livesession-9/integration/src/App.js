import axios from "axios";
import { useEffect, useState } from "react";
import Article from "./components/Article";
import Form from "./components/Form";

function App() {
  const [articles, setArticles] = useState([]);
  const fetchArticles = async () => {
    try {
      // fetch data
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/articles"
      );
      console.log(response.data.data);
      setArticles(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    console.log("Fetch Data");
    fetchArticles();
  }, []);

  return (
    <div>
      <Form fetchArticles={fetchArticles} />
      <h1 className="my-8 text-3xl font-bold text-center">List Article</h1>
      <div className="flex flex-col justify-center gap-6 mt-4 max-w-4xl mx-auto">
        {articles.map((item, index) => {
          /* item = {
            "id": 130,
            "name": "Ini judul artikel Baru 1234",
            "content": "Test",
            "image_url": "https://api-project.amandemy.co.id/images/sepeda.jpg",
            "highlight": false,
            "created_at": "2023-06-09T14:18:03.000000Z",
            "updated_at": "2023-06-09T14:18:03.000000Z",
            "user_id": null,
            "user": null
          
        }
        */
          return <Article data={item} />;
        })}
      </div>
    </div>
  );
}

export default App;
