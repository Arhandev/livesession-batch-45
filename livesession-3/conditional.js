console.log("Program Start");

// if (10 < 5) {
//   console.log("If Statement");
// }

if (10 > 5) {
  console.log("If Statement");
} else if (5 > 2) {
  console.log("Else If Statement");
} else {
  console.log("Else Statement");
}

console.log("Program End");
