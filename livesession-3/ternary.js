var age = 15;

// if (age < 12) {
//   console.log("Kamu masih anak-anak");
// } else if (age == 12) {
//   console.log("Kamu sudah dewasa");
// } else {
//   console.log("Kamu sudah lansia");
// }

age < 12
  ? console.log("Kamu masih anak-anak")
  : age == 12
  ? console.log("Kamu sudah dewasa")
  : console.log("Kamu sudah lansia");

// if (age < 12) {
//   console.log("Kamu anak-anak");
// } else {
//   console.log("Kamu Dewasa");
// }

// age < 12 ? console.log("Kamu anak-anak") : console.log("Kamu Dewasa");

// if (age < 12) {
//   console.log("Kamu anak-anak");
// }

// age < 12 && console.log("Kamu anak-anak");
