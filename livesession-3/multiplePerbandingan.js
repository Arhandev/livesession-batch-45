// console.log(10 > 5 && 16 > 9 && 17 > 15);
//           false      false    false

/*
 anak <= 12 tahun
 Dewasa > 12 dan < 60 tahun
 Lansia >= 60 tahun
*/

var age = 70;

if (age <= 12) {
  console.log("Kamu masih anak-anak");
} else if (age > 12 && age < 60) {
  console.log("Kamu sudah dewasa");
} else {
  console.log("Kamu sudah lansia");
}
