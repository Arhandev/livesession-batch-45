import { useContext, useEffect, useState } from "react";
import Article from "./components/Article";
import Form from "./components/Form";
import TableProduct from "./components/TableProduct";
import UpdateForm from "./components/UpdateForm";
import { ArticleContext } from "./context/ArticleContext";

function App() {
  const { articles, fetchArticles, loading, status } =
    useContext(ArticleContext);
  const [editArticle, setEditArticle] = useState(null);

  useEffect(() => {
    console.log("Fetch Data");
    fetchArticles();
  }, []);

  return (
    <div>
      {status === "error" && (
        <h1 className="text-4xl my-8 font-bold text-center">
          Terjadi Sesuatu Error pada Server
        </h1>
      )}
      {editArticle === null ? (
        <Form />
      ) : (
        <UpdateForm editArticle={editArticle} setEditArticle={setEditArticle} />
      )}

      <TableProduct setEditArticle={setEditArticle} />
      <section>
        <h1 className="my-8 text-3xl font-bold text-center">List Article</h1>
        {loading === false ? (
          <div className="flex flex-col justify-center gap-6 mt-4 max-w-4xl mx-auto">
            {articles.map((item, index) => {
              /* item = {
            "id": 130,
            "name": "Ini judul artikel Baru 1234",
            "content": "Test",
            "image_url": "https://api-project.amandemy.co.id/images/sepeda.jpg",
            "highlight": false,
            "created_at": "2023-06-09T14:18:03.000000Z",
            "updated_at": "2023-06-09T14:18:03.000000Z",
            "user_id": null,
            "user": null
          
        }
        */
              return <Article data={item} />;
            })}
          </div>
        ) : (
          <h1 className="text-center my-6 text-3xl font-bold">Loading ....</h1>
        )}
      </section>
    </div>
  );
}

export default App;
