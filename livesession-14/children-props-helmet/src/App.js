import { BrowserRouter, Route, Routes } from "react-router-dom";
import FormValidation from "./components/FormValidation";
import { ArticleProvider } from "./context/ArticleContext";
import ChildrenPage from "./pages/ChildrenPage";
import CreateArticlePage from "./pages/CreateArticlePage";
import FormPage from "./pages/FormPage";
import LandingPage from "./pages/LandingPage";
import ListArticlePage from "./pages/ListArticlePage";
import TableArticlePage from "./pages/TableArticlePage";
import UpdateArticlePage from "./pages/UpdateArticlePage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <ArticleProvider>
          <Routes>
            <Route path="/" element={<ListArticlePage />} />
            <Route path="/table" element={<TableArticlePage />} />
            <Route path="/create" element={<CreateArticlePage />} />
            <Route path="/form" element={<FormPage />} />
            <Route path="/update/:id" element={<UpdateArticlePage />} />
            <Route path="/children" element={<ChildrenPage />} />
            <Route path="/landing" element={<LandingPage />} />
          </Routes>
        </ArticleProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
