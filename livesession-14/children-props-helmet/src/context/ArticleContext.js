import axios from "axios";
import { createContext, useState } from "react";
import { useNavigate } from "react-router-dom";

export const ArticleContext = createContext();

export const ArticleProvider = ({ children }) => {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState("success");
  const navigate = useNavigate();

  const fetchArticles = async () => {
    try {
      // fetch data
      setLoading(true); // penanda kalau kita mau mengambil data
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/articles"
      );
      console.log(response.data.data);
      setArticles(response.data.data);
      setStatus("success");
    } catch (error) {
      console.log(error);
      setStatus("error");
    } finally {
      // bakal selalu kejalan mau codingan errro atau ngga
      setLoading(false);
    }
  };

  const moveToCreate = () => {
    navigate("/create");
  };

  return (
    <ArticleContext.Provider
      value={{
        articles,
        setArticles,
        loading,
        setLoading,
        status,
        setStatus,
        fetchArticles,
        moveToCreate,
      }}
    >
      {children}
    </ArticleContext.Provider>
  );
};
