import axios from "axios";
import { useFormik } from "formik";
import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { ArticleContext } from "../context/ArticleContext";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama Artikel wajib diisi"),
  content: Yup.string().required("Konten Artikel wajib diisi"),
  highlight: Yup.bool().required("Highlight wajib diisi"),
  image_url: Yup.string()
    .required("Password wajib diisi")
    .url("Link Gambar tidak valid"),
});

function Form() {
  const { fetchArticles } = useContext(ArticleContext);
  const initialState = {
    name: "",
    content: "",
    image_url: "",
    highlight: false,
  };
  const navigate = useNavigate();

  const storeArticle = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/articles",
        {
          name: values.name,
          image_url: values.image_url,
          highlight: values.highlight,
          content: values.content,
        }
      );
      alert("Berhasil Mengirim Request");
      // memannggil data kembali
      fetchArticles();
      resetForm();
      // navigasi ke halaman table
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
  } = useFormik({
    initialValues: initialState,
    onSubmit: storeArticle,
    validationSchema: rulesSchema,
  });

  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-gray-600 p-6 mt-12">
        <h1 className="text-center text-2xl">Form Create Articles</h1>
        <form className="flex flex-col items-center">
          <div className="my-4">
            <label htmlFor="">Nama:</label>
            <input
              type="text"
              placeholder="Masukkan nama"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="name"
              value={values.name}
            />
            <p className="text-red-600 ml-32">
              {touched.name === true && errors.name}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Konten:</label>
            <input
              type="text"
              placeholder="Masukkan Konten"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="content"
              value={values.content}
            />
            <p className="text-red-600 ml-32">
              {touched.content === true && errors.content}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Image URL:</label>
            <input
              type="text"
              placeholder="Masukkan image URL"
              className="pl-2 py-1 ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="image_url"
              value={values.image_url}
            />
            <p className="text-red-600 ml-32">
              {touched.image_url === true && errors.image_url}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Is Highlight:</label>
            <input
              type="checkbox"
              className="ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="highlight"
              checked={values.highlight}
            />
            <p className="text-red-600 ml-32">
              {touched.highlight === true && errors.highlight}
            </p>
          </div>
          <button
            type="button"
            onClick={handleSubmit}
            className="px-6 py-2 mt-8 bg-blue-600 text-white rounded-md"
          >
            Submit
          </button>
        </form>
      </section>
    </div>
  );
}

export default Form;
