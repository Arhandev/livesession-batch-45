import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <header className="shadow-lg py-4 bg-white px-12 sticky top-0">
      <nav className="mx-auto max-w-7xl flex justify-between">
        <div></div>
        <ul className="flex items-center gap-6 text-cyan-500 text-xl">
          <Link to="/">
            <li>Home</li>
          </Link>
          <Link to="/table">
            <li>Table</li>
          </Link>
          <Link to="/form">
            <li>Form</li>
          </Link>
          <Link to="/children">
            <li>Children</li>
          </Link>
        </ul>
        <div className="flex items-center gap-4"></div>
      </nav>
    </header>
  );
}

export default Navbar;
