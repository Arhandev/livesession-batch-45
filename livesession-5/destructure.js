// const arr = ["Farhan", "Abdul", "Hamid"];

// const [firstValue, secondValue, thirdValue, fourthValue] = arr;
// const [, , third] = arr;

// console.log(firstValue);
// console.log(secondValue);
// console.log(thirdValue);
// console.log(fourthValue);

// const arr = ["Farhan", "Abdul", "Hamid"];

// const [first, ...sisa] = arr;

// console.log(sisa);

const obj = {
  first_name: "Farhan",
  last_name: "Abdul Hamid",
  domisili: "Jakarta",
};

const { first_name: firstName} = obj;

console.log(firstName);
