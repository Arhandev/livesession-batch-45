const arr1 = [1, 2, 3, 4, 5, 6];
const arr2 = [4, 5, 6];

const result = [...arr2, ...arr1];

console.log(result);

// const obj1 = {
//   first_name: "farhan",
//   last_name: "abdul hamid",
// };

// const obj2 = {
//   domisili: "Jakarta",
//   first_name: "Rayhan",
// };

// const result = {
//   ...obj1,
//   ...obj2,
// };

// console.log(result);
