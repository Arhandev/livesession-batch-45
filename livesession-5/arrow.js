/*
function print(params1, params2, params3) {
  console.log("Halo Dunia");
  return params1;
}

const printArrow = (params1, params2, params3) => {
  console.log("Halo Dunia");
  return params1;
};

print();
printArrow();
*/

// function luas(panjang, lebar) {
//   return panjang * lebar;
// }

// const luasArrow = (panjang, lebar) => panjang * lebar;

function luas(sisi) {
  return sisi;
}

// const luasArrow = sisi => sisi;
const luasArrow = (panjang, lebar) => panjang * lebar;

const result = luas(5, 10);
const resultArrow = luasArrow(5, 10);

console.log(result);
console.log(resultArrow);
