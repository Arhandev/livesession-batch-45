// const arr = [3, 2, 5, 7];

// const arrFilter = arr.filter((item, index)=>{
//     return item > 6
// })

// console.log(arrFilter);

const arrObj = [
    {
        name: "Farhan",
        nilai: 80
    },
    {
        name: "Rayhan",
        nilai: 60
    },
    {
        name: "Adit",
        nilai: 75
    },
]

const arrSiswaLulus = arrObj.filter((item, index)=>{
    return item.nilai > 75
})

console.log(arrSiswaLulus);