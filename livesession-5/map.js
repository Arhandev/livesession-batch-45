const arr = [3, 2, 5, 7];

const arrMapped = arr.map((item, index) => {
  if (index > 2) {
    return item * 4;
  }

  return item;
});

console.log(arrMapped);
