const restExample = (...params) => {
  console.log(params);
};

restExample("Halo Dunia");
restExample("Halo Dunia", "Nama aku Farhan");
restExample("Halo Dunia", "Nama aku Farhan", 2000);
restExample("Halo Dunia", "Nama aku Farhan", 2000, 4000, 5000);
restExample()