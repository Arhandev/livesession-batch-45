import { useFormik } from "formik";
import React from "react";
import * as Yup from "yup";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama wajib diisi"),
  alamat: Yup.string().required("Alamat wajib diisi"),
  password: Yup.string()
    .required("Password wajib diisi")
    .min(8, "Minimal harus 8 Huruf"),
});

function FormValidation() {
  const initialState = {
    name: "",
    alamat: "",
    password: "",
  };

  const onSubmit = (values) => {
    console.log("Method berjalan");
    //  melakukan request ke server
    console.log(values);
  };

  const print = (values) => {
    // menampilkan inputan user
    console.log(values);
  };

  const { handleChange, handleSubmit, errors, handleBlur, touched } = useFormik(
    {
      initialValues: initialState,
      onSubmit: onSubmit,
      validationSchema: rulesSchema,
    }
  );

  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-gray-600 p-6 mt-12">
        <h1 className="text-center text-2xl">Form Create Data</h1>
        <form className="flex flex-col items-center">
          <div className="my-4">
            <label htmlFor="">Nama Lengkap:</label>
            <input
              type="text"
              placeholder="Masukkan nama"
              className="pl-2 py-1 ml-4 border-2 border-gray-600 rounded-md"
              name="name"
              onChange={handleChange}
              onBlur={handleBlur}
            />
            <p className="text-red-600 ml-32">
              {touched.name === true && errors.name}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Alamat:</label>
            <input
              type="text"
              placeholder="Masukkan Address"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              name="alamat"
              onChange={handleChange}
              onBlur={handleBlur}
            />
            <p className="text-red-600 ml-32">
              {touched.alamat === true && errors.alamat}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Password:</label>
            <input
              type="password"
              placeholder="Masukkan password"
              className="pl-2 py-1 ml-4 border-2 border-gray-600 rounded-md"
              name="password"
              onChange={handleChange}
              onBlur={handleBlur}
            />
            <p className="text-red-600 ml-32">
              {touched.password === true && errors.password}
            </p>
          </div>
          <button
            onClick={handleSubmit}
            type="button"
            className="px-6 py-2 mt-8 bg-blue-600 text-white rounded-md"
          >
            Submit
          </button>

          <button
            onClick={print}
            type="button"
            className="px-6 py-2 mt-8 bg-green-600 text-white rounded-md"
          >
            Test Function
          </button>
        </form>
      </section>
    </div>
  );
}

export default FormValidation;
