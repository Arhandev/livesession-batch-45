import React from "react";
import FormValidation from "../components/FormValidation";
import Navbar from "../components/Navbar";

function FormPage() {
  return (
    <div>
      <Navbar />
      <FormValidation />
    </div>
  );
}

export default FormPage;
