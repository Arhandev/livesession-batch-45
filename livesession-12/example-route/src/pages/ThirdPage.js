import React from "react";
import Navbar from "../components/Navbar";

function ThirdPage() {
  return (
    <div>
      <Navbar />
      <h1 className="text-center text-4xl font-bold my-16">Halaman Ketiga</h1>
    </div>
  );
}

export default ThirdPage;
