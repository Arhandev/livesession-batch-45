import React from "react";
import Navbar from "../components/Navbar";

function FirstPage() {
  return (
    <div>
      <Navbar />
      <h1 className="text-center text-4xl font-bold my-16">Halaman Pertama</h1>
    </div>
  );
}

export default FirstPage;
