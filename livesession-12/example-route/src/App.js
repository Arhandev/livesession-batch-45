import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import DynamicPage from "./pages/DynamicPage";
import FirstPage from "./pages/FirstPage";
import SecondPage from "./pages/SecondPage";
import ThirdPage from "./pages/ThirdPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<FirstPage />} />
          <Route path="/two" element={<SecondPage />} />
          <Route path="/three" element={<ThirdPage />} />
          <Route path="/dynamic/:slug" element={<DynamicPage />} />
          <Route path="/dynamic/:slug/:id" element={<h1>Halo DUnia</h1>} />
        </Routes>
      </BrowserRouter>

      {/* <FirstPage />
      <SecondPage />
      <ThirdPage /> */}
    </div>
  );
}

export default App;
