import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ArticleProvider } from "./context/ArticleContext";
import CreateArticlePage from "./pages/CreateArticlePage";
import ListArticlePage from "./pages/ListArticlePage";
import TableArticlePage from "./pages/TableArticlePage";
import UpdateArticlePage from "./pages/UpdateArticlePage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <ArticleProvider>
          <Routes>
            <Route path="/" element={<ListArticlePage />} />
            <Route path="/table" element={<TableArticlePage />} />
            <Route path="/create" element={<CreateArticlePage />} />
            <Route path="/update/:id" element={<UpdateArticlePage />} />
          </Routes>
        </ArticleProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
