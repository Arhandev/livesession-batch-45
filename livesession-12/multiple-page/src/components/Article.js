import React from "react";

function Article({ data }) {
  return (
    <div className="flex gap-4 border-2 border-black rounded-md p-4">
      <div>
        <img src={data.image_url} alt="" className="w-64 h-64 object-cover" />
      </div>
      <div className="">
        <h1 className={"text-black text-2xl font-bold"}>{data.name}</h1>
        <p className="text-lg mt-4">{data.content}</p>
      </div>
    </div>
  );
}

export default Article;
