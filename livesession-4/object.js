var object = {
  name: "farhan",
  "last name": "Abdul Hamid",
  late_name: "test",
};

// cara 1
// console.log(object.last name);

// cara 2
// console.log(object["last name"]);

object.name = "aan";
object.country = "Indonesia";

delete object['last name']

console.log(object);
