import { useState } from "react";

function App() {
  const name = ["Farhan", "Abdul", "Hamid"];

  // let text = "Hello World";
  const [text, setText] = useState("Hello World");
  const [text2, setText2] = useState(["Farhan", "Abdul", "Hamid"]);
  const [data, setData] = useState({
    students: [
      {
        name: "farhan",
        domisili: "jakarta",
      },
      {
        name: "rayhan",
        domisili: "bandung",
      },
    ],
    nama_sekolah: "Tadika",
    kepala_sekolah: "Budi",
  });

  const popAlert = () => {
    console.log("Kamu klik button");
  };

  const changeText = () => {
    // text = "Halo Dunia";
    const test = "Farhan";
    setText(`Halo Dunia ${text2}`);
  };

  return (
    <div>
      <h1>{data.kepala_sekolah}</h1>
      <button onClick={changeText}>Klik aku</button>
      {text2.map((item, index) => {
        return (
          <h1>
            {index}. {item}
          </h1>
        );
      })}
    </div>
  );
}

export default App;
