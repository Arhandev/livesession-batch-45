import { Button } from "antd";
import axios from "axios";
import React from "react";
import { Link, useNavigate } from "react-router-dom";

function Navbar() {
  const navigate = useNavigate();
  const onLogout = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/logout",
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    } finally {
      // penghapusan localstorage
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      navigate("/login");
    }
  };
  return (
    <header className="shadow-lg py-4 bg-white px-12 sticky top-0 z-20">
      <nav className="mx-auto max-w-7xl flex justify-between">
        <div></div>
        <ul className="flex items-center gap-6 text-cyan-500 text-xl list-none">
          <Link className="no-underline" to="/">
            <li>Home</li>
          </Link>
          <Link className="no-underline" to="/table">
            <li>Table</li>
          </Link>
          <Link className="no-underline" to="/form">
            <li>Form</li>
          </Link>
          <Link className="no-underline" to="/children">
            <li>Children</li>
          </Link>
        </ul>
        <div className="flex items-center gap-6 text-cyan-500 text-xl list-none">
          {localStorage.getItem("token") != null ? (
            <>
              <h1>Hai, {localStorage.getItem("username")}</h1>
              <Button onClick={onLogout} type="primary" danger>
                Logout
              </Button>
            </>
          ) : (
            <>
              <Link className="no-underline" to="/login">
                <Button type="primary">Login</Button>
              </Link>
              <Link className="no-underline" to="/register">
                <Button>Register</Button>
              </Link>
            </>
          )}
        </div>
      </nav>
    </header>
  );
}

export default Navbar;
