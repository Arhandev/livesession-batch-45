import { Button, Form, Input } from "antd";
import axios from "axios";
import { useFormik } from "formik";
import React from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama Pengguna wajib diisi"),
  username: Yup.string().required("Username Pengguna wajib diisi"),
  email: Yup.string()
    .required("Email Pengguna wajib diisi")
    .email("Email tidak valid"),
  password: Yup.string().required("Password wajib diisi"),
  password_confirmation: Yup.string().required(
    "Password Konfirmasi wajib diisi"
  ),
});

function RegisterForm() {
  const navigate = useNavigate();

  const initialState = {
    name: "",
    username: "",
    email: "",
    password: "",
    password_confirmation: "",
  };

  const onRegister = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/register",
        {
          name: values.name,
          username: values.username,
          email: values.email,
          password: values.password,
          password_confirmation: values.password_confirmation,
        }
      );
      alert("Berhasil Melakukan Regis");
      resetForm();
      // navigasi ke halaman table
      navigate("/login");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: initialState,
    onSubmit: onRegister,
    validationSchema: rulesSchema,
  });

  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-solid border-gray-600 p-6 mt-12">
        <h1 className="text-center text-2xl">Form Register</h1>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          className="max-w-2xl"
        >
          <Form.Item
            label="Nama"
            name="name"
            help={touched.name === true && errors.name}
            hasFeedback={true}
            validateStatus={errors.name === true && "errors"}
          >
            <Input
              type="text"
              placeholder="Masukkan nama pengguna"
              onChange={handleChange}
              onBlur={handleBlur}
              name="name"
              value={values.name}
            />
          </Form.Item>
          <Form.Item
            label="Username"
            name="username"
            help={touched.username === true && errors.username}
            hasFeedback={true}
            validateStatus={errors.username === true && "errors"}
          >
            <Input
              type="text"
              placeholder="Masukkan username pengguna"
              onChange={handleChange}
              onBlur={handleBlur}
              name="username"
              value={values.username}
            />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            help={touched.email === true && errors.email}
            hasFeedback={true}
            validateStatus={errors.email === true && "errors"}
          >
            <Input
              type="text"
              placeholder="Masukkan email pengguna"
              onChange={handleChange}
              onBlur={handleBlur}
              name="email"
              value={values.email}
            />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            help={touched.password === true && errors.password}
            hasFeedback={true}
            validateStatus={errors.password === true && "errors"}
          >
            <Input.Password
              type="text"
              placeholder="Masukkan password"
              onChange={handleChange}
              onBlur={handleBlur}
              name="password"
              value={values.password}
            />
          </Form.Item>

          <Form.Item
            label="Konfirmasi Password"
            name="password_confirmation"
            help={
              touched.password_confirmation === true &&
              errors.password_confirmation
            }
            hasFeedback={true}
            validateStatus={errors.password_confirmation === true && "errors"}
          >
            <Input.Password
              type="text"
              placeholder="Masukkan password"
              onChange={handleChange}
              onBlur={handleBlur}
              name="password_confirmation"
              value={values.password_confirmation}
            />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" onClick={handleSubmit}>
              Register
            </Button>
          </Form.Item>
        </Form>
      </section>
    </div>
  );
}

export default RegisterForm;
