import { Button, Form, Input } from "antd";
import axios from "axios";
import { useFormik } from "formik";
import React from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";

const rulesSchema = Yup.object({
  email: Yup.string()
    .required("Email Pengguna wajib diisi")
    .email("Email tidak valid"),
  password: Yup.string().required("Password wajib diisi"),
});

function LoginForm() {
  const navigate = useNavigate();

  const initialState = {
    email: "",
    password: "",
  };

  const onLogin = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/login",
        {
          email: values.email,
          password: values.password,
        }
      );

      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("username", response.data.data.user.username);

      alert("Berhasil Melakukan Login");
      resetForm();
      // navigasi ke halaman table
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: initialState,
    onSubmit: onLogin,
    validationSchema: rulesSchema,
  });

  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-solid border-gray-600 p-6 mt-12">
        <h1 className="text-center text-2xl">Form Login</h1>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          className="max-w-2xl"
        >
          <Form.Item
            label="Email"
            name="email"
            help={touched.email === true && errors.email}
            hasFeedback={true}
            validateStatus={errors.email === true && "errors"}
          >
            <Input
              type="text"
              placeholder="Masukkan email pengguna"
              onChange={handleChange}
              onBlur={handleBlur}
              name="email"
              value={values.email}
            />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            help={touched.password === true && errors.password}
            hasFeedback={true}
            validateStatus={errors.password === true && "errors"}
          >
            <Input.Password
              type="text"
              placeholder="Masukkan password"
              onChange={handleChange}
              onBlur={handleBlur}
              name="password"
              value={values.password}
            />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" onClick={handleSubmit}>
              Login
            </Button>
          </Form.Item>
        </Form>
      </section>
    </div>
  );
}

export default LoginForm;
