import { Button, ConfigProvider, Table } from "antd";
import axios from "axios";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { ArticleContext } from "../context/ArticleContext";

function TableProduct() {
  const { articles, fetchArticles, moveToCreate } = useContext(ArticleContext);

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Dibuat Oleh",
      dataIndex: "author",
      key: "author",
      render: (_, record) => {
        if (record.user === null) {
          return <>-</>;
        }
        return <>{record.user.username}</>;
      },
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Content",
      dataIndex: "content",
      key: "content",
    },
    {
      title: "Image",
      dataIndex: "image_url",
      key: "image_url",
      render: (_, record) => {
        return (
          <img
            className="w-64"
            src={record.image_url}
            alt={record.name + " Image"}
          />
        );
      },
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (_, record) => {
        return (
          <div className="flex gap-2">
            <Link to={`/update/${record.id}`}>
              <ConfigProvider
                theme={{
                  token: {
                    colorPrimary: "#da8e01",
                  },
                }}
              >
                <Button type="primary">Update</Button>
              </ConfigProvider>
            </Link>
            <ConfigProvider
              theme={{
                token: {
                  colorPrimary: "#b90000",
                },
              }}
            >
              <Button type="primary" onClick={() => onDelete(record.id)}>
                Delete
              </Button>
            </ConfigProvider>
          </div>
        );
      },
    },
  ];

  const onUpdate = (article) => {
    // alert(id);
  };
  
  const onDelete = async (id) => {
    // alert(`Berhasil Menghapus Artikel dengan ID: ${id}`);
    try {
      // fetch data
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/articles/${id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      alert("Berhasil Menghapus Product");
      fetchArticles();
    } catch (error) {
      console.log(error);
      alert("Gagal Menghapus Product");
    }
  };
  return (
    <section>
      <h1 className="my-8 text-3xl font-bold text-center">Table Article</h1>
      <div className="max-w-4xl mx-auto w-full my-4">
        <Link to="/create">
          <Button type="primary">Buat Artikel +</Button>
        </Link>
        <ConfigProvider
          theme={{
            components: {
              Button: {
                colorPrimary: "green",
                colorPrimaryHover: "#07fb07",
              },
            },
          }}
        >
          <Button type="primary" onClick={moveToCreate}>
            Buat Artikel +
          </Button>
        </ConfigProvider>

        <Table columns={columns} dataSource={articles} />
      </div>
    </section>
  );
}

export default TableProduct;
