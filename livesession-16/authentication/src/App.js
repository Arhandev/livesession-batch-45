import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ArticleProvider } from "./context/ArticleContext";
import ChildrenPage from "./pages/ChildrenPage";
import CreateArticlePage from "./pages/CreateArticlePage";
import FormPage from "./pages/FormPage";
import LandingPage from "./pages/LandingPage";
import ListArticlePage from "./pages/ListArticlePage";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import TableArticlePage from "./pages/TableArticlePage";
import UIPage from "./pages/UIPage";
import UpdateArticlePage from "./pages/UpdateArticlePage";
import ProtectedRoute from "./wrapper/ProtectedRoute";
import GuestRoute from "./wrapper/GuestRoute";

function App() {
  return (
    <div>
      <BrowserRouter>
        <ArticleProvider>
          <Routes>
            <Route path="/" element={<ListArticlePage />} />
            <Route path="/form" element={<FormPage />} />
            <Route path="/update/:id" element={<UpdateArticlePage />} />
            <Route path="/children" element={<ChildrenPage />} />
            <Route path="/landing" element={<LandingPage />} />
            <Route path="/ui-kit" element={<UIPage />} />

            <Route element={<ProtectedRoute />}>
              <Route path="/table" element={<TableArticlePage />} />
              <Route path="/create" element={<CreateArticlePage />} />
            </Route>

            <Route element={<GuestRoute />}>
              <Route path="/login" element={<LoginPage />} />
              <Route path="/register" element={<RegisterPage />} />
            </Route>
          </Routes>
        </ArticleProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
