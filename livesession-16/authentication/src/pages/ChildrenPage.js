import React, { useState } from "react";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import WrapperComponent from "../components/WrapperComponent";

function ChildrenPage() {
  const [title, setTitle] = useState("Children Page");

  const changeTitle = () => {
    setTitle("Title berubah");
  };

  return (
    <div>
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <Layout>
        <div className="flex justify-center items-center my-8">
          <button
            onClick={changeTitle}
            className="px-8 py-4 bg-red-600 text-white"
          >
            Ganti Judul
          </button>
        </div>
        <WrapperComponent>
          <div className="p-16 bg-red-600">
            <h1 className="text-white text-center">Children Content 1</h1>
          </div>
        </WrapperComponent>
        <WrapperComponent>
          <div className="p-16 bg-green-600">
            <h1 className="text-white text-center">Children Content 2</h1>
          </div>
        </WrapperComponent>
      </Layout>
    </div>
  );
}

export default ChildrenPage;
