console.log("Program Mulai");

setTimeout(() => {
  console.log("Fungsi yang berjalan 1 detik setelahnya");
}, 0);

setTimeout(() => {
  console.log("=====================");
  console.log("Set Timeout 2");
  console.log("Fungsi yang berjalan 3 detik setelahnya");
}, 0);

console.log("Program selesai");

for (let i = 0; i < 100; i++) {
  console.log(i);
}
