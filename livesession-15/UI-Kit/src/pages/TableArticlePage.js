import React, { useContext, useEffect } from "react";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import TableProduct from "../components/TableProduct";
import { ArticleContext } from "../context/ArticleContext";

function TableArticlePage() {
  const { fetchArticles } = useContext(ArticleContext);
  useEffect(() => {
    fetchArticles();
  }, []);
  return (
    <div>
      <Helmet>
        <title>Table Article Page</title>
      </Helmet>
      <Layout>
        <TableProduct />
      </Layout>
    </div>
  );
}

export default TableArticlePage;
