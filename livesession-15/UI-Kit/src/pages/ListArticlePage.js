import React, { useContext, useEffect } from "react";
import { Helmet } from "react-helmet";
import Article from "../components/Article";
import Layout from "../components/Layout";
import { ArticleContext } from "../context/ArticleContext";

function ListArticlePage() {
  const { articles, fetchArticles, loading, status } =
    useContext(ArticleContext);

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <section>
      <Helmet>
        <title>List Article Page</title>
      </Helmet>
      <Layout>
        <h1 className="my-8 text-3xl font-bold text-center">List Article</h1>
        {loading === false ? (
          <div className="flex flex-col justify-center gap-6 mt-4 max-w-4xl mx-auto">
            {articles.map((item, index) => {
              /* item = {
            "id": 130,
            "name": "Ini judul artikel Baru 1234",
            "content": "Test",
            "image_url": "https://api-project.amandemy.co.id/images/sepeda.jpg",
            "highlight": false,
            "created_at": "2023-06-09T14:18:03.000000Z",
            "updated_at": "2023-06-09T14:18:03.000000Z",
            "user_id": null,
            "user": null
          
        }
        */
              return <Article data={item} />;
            })}
          </div>
        ) : (
          <h1 className="text-center my-6 text-3xl font-bold">Loading ....</h1>
        )}
      </Layout>
    </section>
  );
}

export default ListArticlePage;
