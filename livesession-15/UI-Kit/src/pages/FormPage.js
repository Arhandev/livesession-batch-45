import React from "react";
import { Helmet } from "react-helmet";
import FormValidation from "../components/FormValidation";
import Layout from "../components/Layout";

function FormPage() {
  return (
    <div>
      <Helmet>
        <title>Form Page</title>
      </Helmet>
      <Layout>
        <FormValidation />
      </Layout>
    </div>
  );
}

export default FormPage;
