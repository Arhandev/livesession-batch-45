import React from "react";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import UpdateForm from "../components/UpdateForm";

function UpdateArticlePage() {
  return (
    <div>
      <Helmet>
        <title>Update Article Page</title>
      </Helmet>
      <Layout>
        <UpdateForm />
      </Layout>
    </div>
  );
}

export default UpdateArticlePage;
