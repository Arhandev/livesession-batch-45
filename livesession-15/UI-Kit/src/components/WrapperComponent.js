import React from 'react'

function WrapperComponent({children}) {
  return (
    <div className='my-16 bg-blue-600 p-12 max-w-4xl mx-auto'>
        {children}
        <h1 className='text-3xl text-white text-center font-bold my-10'>
            Wrapper Component
        </h1>
    </div>
  )
}

export default WrapperComponent