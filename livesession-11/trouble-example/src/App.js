import { useContext } from "react";
import "./App.css";
import FirstComponent from "./components/FirstComponent";
import { GlobalContext } from "./context/GlobalContext";

function App() {
  const { kalimat, setKalimat1 } = useContext(GlobalContext);

  const handleClick = () => {
    setKalimat1("Hello World");
  };

  return (
    <div className="m-4 p-4 bg-black text-white">
      <h1>App Component</h1>
      <button
        onClick={handleClick}
        className="px-8 py-4 bg-red-600 text-white border border-white text-lg"
      >
        Klik Aku
      </button>
      <h1>{kalimat}</h1>
      <FirstComponent />
    </div>
  );
}

export default App;
