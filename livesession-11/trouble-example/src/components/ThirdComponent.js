import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalContext";

function ThirdComponent() {
  const { kalimat, kalimat1 } = useContext(GlobalContext);

  return (
    <div className="m-4 p-4 bg-blue-600 text-white">
      <h1 className="text-lg">Third Component</h1>
      <p className="text-xl text-black font-bold">{kalimat}</p>
      <p className="text-3xl text-white font-bold">{kalimat1}</p>
    </div>
  );
}

export default ThirdComponent;
