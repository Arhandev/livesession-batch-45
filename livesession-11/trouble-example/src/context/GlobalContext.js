import { createContext, useState } from "react";

export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  const text = "Halo nama saya Rayhan";
  const [kalimat1, setKalimat1] = useState("Halo Dunia");

  return (
    <GlobalContext.Provider
      value={{ kalimat: text, kalimat1: kalimat1, setKalimat1: setKalimat1 }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
