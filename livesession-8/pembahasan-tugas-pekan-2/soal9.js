function luasPersegiPanjang(panjang, lebar) {
  return panjang * lebar;
}

var luas1 = luasPersegiPanjang(10, 5);
var luas2 = luasPersegiPanjang(4, 4);

console.log(luas1);
console.log(luas2);
