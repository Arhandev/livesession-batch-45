function kuadrat(nilai) {
  return nilai * nilai;
}

function bodyMassIndex(berat, tinggi) {
  var tinggiKuadrat = kuadrat(tinggi);
  return berat / tinggiKuadrat;
}


var result = bodyMassIndex(80, 1.80);
console.log(result);