
// cara 1
for (var i = 0; i < 20; i += 3) {
  console.log(i);
}

// cara 2 (recommend)
for(var i = 0; i < 20; i++){
    if(i % 3 === 0){
        console.log(i);
    }
}
