// cara 1
for (var i = 1; i < 15; i += 2) {
  console.log(i);
}

// cara 2 (recommend)
for (var i = 0; i < 15; i++) {
    if(i % 2 === 1){
        console.log(i);
    }
}
