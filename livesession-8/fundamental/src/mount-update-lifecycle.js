import { useEffect, useState } from "react";

function App() {
  // mounting use Effect
  // useEffect(() => {
  //   console.log("Component telah di mount");
  //   console.log("Halo Dunia");
  //   // fetch Data
  // }, []);

  const [counter, setCounter] = useState(0);

  const handlePlus = () => {
    setCounter(counter + 1);
  };

  const handleMinus = () => {
    setCounter(counter - 1);
  };

  useEffect(() => {
    console.log("Update Life Cycle");
  }, [counter]);

  return (
    <div>
      <h1 className="text-5xl text-red-500">Halo Dunia</h1>
      <div className="flex items-center justify-center gap-8">
        <button
          onClick={handleMinus}
          className="px-6 py-3 border-2 border-black bg-red-700 text-white"
        >
          Minus
        </button>
        <p className="text-3xl">{counter}</p>
        <button
          onClick={handlePlus}
          className="px-6 py-3 border-2 border-black bg-green-700 text-white"
        >
          Plus
        </button>
      </div>
    </div>
  );
}

export default App;
