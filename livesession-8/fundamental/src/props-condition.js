import Header from "./components/Header";

function App() {
  const name = "Farhan Abdul Hamid";
  const age = 15;
  const popAlert = () => {
    alert("Halo Dunia");
  };

  return (
    <div>
      <h1 className="text-5xl text-red-500">Halo Dunia</h1>
      <h1>Nama Saya Farhan</h1>
      <h2>Hello</h2>
      {age > 17 ? (
        <Header data={name} func={popAlert} />
      ) : (
        <h1>Tidak dapat menampilkan Data</h1>
      )}

      {age > 17 && (
        <h1 className="text-7xl text-blue-700">
          Selamat Kamu sudah cukup umur
        </h1>
      )}
    </div>
  );
}

export default App;
