import { useState } from "react";
import Header from "./components/Header";

function App() {
  const [display, setDisplay] = useState(false);

  const changeDisplay = () => {
    setDisplay(!display);
  };

  return (
    <div>
      <h1 className="text-5xl text-red-500">
        Conditional Rendering dan Unmount
      </h1>
      <div className="flex justify-center my-5">
        <button
          onClick={changeDisplay}
          className="px-5 py-3 bg-green-700 text-white text-2xl border-2 border-black"
        >
          Show/Hide
        </button>
      </div>
      {display && <Header />}
    </div>
  );
}

export default App;
