import React from "react";

function Header(props) {
  return (
    <div>
      <h1 className="text-7xl text-green-300 bg-black">{props.data}</h1>
      <button
        className="px-6 py-2 bg-blue-700 text-white my-10"
        onClick={props.func}
      >
        Klik aku
      </button>
    </div>
  );
}

export default Header;
