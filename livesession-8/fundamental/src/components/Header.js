import React, { useEffect } from "react";

function Header() {
  useEffect(() => {
    console.log("Mounting Header");

    return ()=>{
      console.log("Unmounting Header");
    }
  }, []);
  return (
    <div>
      <div className="max-w-7xl mx-auto bg-red-700 text-white text-center py-20 my-12 text-4xl">
        Header Component
      </div>
    </div>
  );
}

export default Header;
