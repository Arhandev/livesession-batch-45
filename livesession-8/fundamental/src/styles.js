import Header from "./components/Header";
import styles from "./style.module.css";

function App() {
  return (
    <div>
      <h1 style={{ fontSize: "50px", color: "blue", backgroundColor: "red" }}>
        Halo Dunia
      </h1>
      <h1 className={styles.first_header}>Nama Saya Farhan</h1>
      <h2 id={styles.unique_header}>Hello</h2>
      <Header />
    </div>
  );
}

export default App;
